
		var cartCount = 0;
		var cartContent = [];

		function showCart() {
			var cartText = "";
			for (var i = 0; i < cartContent.length; i++) {
				var item = cartContent[i];
				var count = 1;
				for (var j = i + 1; j < cartContent.length; j++) {
					if (cartContent[j] === item) {
						count++;
						cartContent.splice(j, 1);
						j--;
					}
				}
				cartText += count + "x " + item + "<br>";
			}
			document.getElementById("cart-content").innerHTML = cartText;
		}

		function toggleCart() {
			var cartContentDiv = document.getElementById("cart-content");
			if (cartContentDiv.style.display === "block") {
				cartContentDiv.style.display = "none";
			} else {
				showCart();
				cartContentDiv.style.display = "block";
			}
		}

		document.getElementById("button1").addEventListener("click", function() {
            cartCount++;
            cartContent.push("Button 1");
            if (cartCount > 0) {
                document.getElementById("cart").innerHTML = "🛒" + cartCount;
            }
        });
        
        document.getElementById("button2").addEventListener("click", function() {
            cartCount++;
            cartContent.push("Button 2");
            if (cartCount > 0) {
                
                document.getElementById("cart").innerHTML = "🛒" + cartCount;
            }
        });